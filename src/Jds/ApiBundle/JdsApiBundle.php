<?php

namespace Jds\ApiBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class JdsApiBundle extends Bundle
{
	public function getParent()
    {
        return 'FOSUserBundle';
    }
}

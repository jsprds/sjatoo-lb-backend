<?php

namespace Jds\ApiBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OrderType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('orderDate', 'datetime', array('widget' => 'single_text'));
        $builder->add('status', 'checkbox', array(
            'required'  => false
        ));
        $builder->add('orderDetails', 'collection', array(
            'type' => new OrderDetailType(), 
            'allow_add' => true,
            'by_reference'  => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'Jds\ApiBundle\Entity\Order',
            'csrf_protection'   => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'order';
    }
}
<?php

/**
 * @author Jasper De Smet <jasp.desmet@gmail.com>
 * @version 1.0
 * @package Jds\Helpers
 */

namespace Jds\ApiBundle\Helper;

class ImageUploader {

    private $uploadDir;

    public function setUploadDir($dir) {
        $this->uploadDir = $dir;
    }

    public function getUploadDir() {
        return $this->uploadDir;
    }

    public function __construct($dir) {
        $this->setUploadDir($dir);
    }

    public function base64Image($img) {
        $data = base64_decode($img);
        $imageName = date("Ymd_H") . "_" . uniqid() . '.png';
        $file = $this->getUploadDir() .  $imageName;
        $success = file_put_contents($file, $data);
        return $imageName;
    }
    
}
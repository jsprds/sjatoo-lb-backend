<?php

namespace Jds\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Een prijs kan gebruikt worden door meerdere producten (Voorbeeld: €1 voor cola, pintjes, ...) dus niet gekoppeld aan één product maar kan aan meerdere producten gekoppeld worden
 * 
 * @ORM\Entity()
 */
class Price
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", length=10)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2)
     */
    protected $price;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Price
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }
}

<?php

namespace Jds\ApiBundle\Entity;

use Jds\ApiBundle\Model\CheckinInterface;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity()
 * @ORM\Table(name="checkin")
 */
class Checkin implements CheckinInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", length=6)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $checkinDate;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    private $user;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Checkin
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return Checkin
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return Checkin
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set memberPrice
     *
     * @param string $memberPrice
     * @return Checkin
     */
    public function setMemberPrice($memberPrice)
    {
        $this->memberPrice = $memberPrice;

        return $this;
    }

    /**
     * Get memberPrice
     *
     * @return string 
     */
    public function getMemberPrice()
    {
        return $this->memberPrice;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Checkin
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set facebookUrl
     *
     * @param string $facebookUrl
     * @return Checkin
     */
    public function setFacebookUrl($facebookUrl)
    {
        $this->facebookUrl = $facebookUrl;

        return $this;
    }

    /**
     * Get facebookUrl
     *
     * @return string 
     */
    public function getFacebookUrl()
    {
        return $this->facebookUrl;
    }

    /**
     * Set checkinDate
     *
     * @param \DateTime $checkinDate
     * @return Checkin
     */
    public function setCheckinDate($checkinDate)
    {
        $this->checkinDate = $checkinDate;

        return $this;
    }

    /**
     * Get checkinDate
     *
     * @return \DateTime 
     */
    public function getCheckinDate()
    {
        return $this->checkinDate;
    }

    /**
     * Set user
     *
     * @param \Jds\ApiBundle\Entity\User $user
     * @return Checkin
     */
    public function setUser(\Jds\ApiBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Jds\ApiBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}

<?php
// src/Acme/UserBundle/Entity/User.php

namespace Jds\ApiBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Jds\ApiBundle\Model\UserInterface;


/**
 * @ORM\Entity
 * @ORM\Table(name="the_user")
 * @UniqueEntity(fields="rfid", message="RFID zit al in DB", groups={"rfid_registration"})
 * @ORM\AttributeOverrides({
 *      @ORM\AttributeOverride(name="email", column=@ORM\Column(unique=false, nullable=true)),
 *      @ORM\AttributeOverride(name="emailCanonical", column=@ORM\Column(unique=false, nullable=true)),
 *      @ORM\AttributeOverride(name="username", column=@ORM\Column(unique=false, nullable=true)),
 *      @ORM\AttributeOverride(name="usernameCanonical", column=@ORM\Column(unique=false, nullable=true)),
 *      @ORM\AttributeOverride(name="password", column=@ORM\Column(nullable=true))
 * })
 */
class User extends BaseUser implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"userDetails", "userList"})
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=10, unique=true)
     * @Serializer\Groups({"userDetails", "userList"})
     */
    protected $rfid;

    /**
     * @ORM\Column(type="string", length=80)
     * @Serializer\Groups({"userDetails", "userList"})
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", length=80)
     * @Serializer\Groups({"userDetails", "userList"})
     */
    protected $lastname;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Serializer\Groups({"userDetails"})
     */
    protected $phone;

    /**
     * @ORM\Column(type="date")
     * @Serializer\Groups({"userList", "userDetails"})
     */
    protected $dateOfBirth;

    /**
     * @ORM\Column(type="string", length=140)
     * @Serializer\Groups({"userList", "userDetails"})
     */
    protected $street;

    /**
     * @ORM\Column(type="string", length=4)
     * @Serializer\Groups({"userList", "userDetails"})
     */
    protected $zipcode;

     /**
     * @ORM\Column(type="string", length=80)
     * @Serializer\Groups({"userList", "userDetails"})
     */
    protected $city;

    /**
     * @ORM\Column(name="gender", type="string", columnDefinition="enum('male', 'female')")
     * @Serializer\Groups({"userList", "userDetails"})
     */
    protected $gender;

    /**
     * @ORM\Column(type="string", length=140)
     * @Serializer\Groups({"userList", "userDetails"})
     */
    protected $imageUrl;

    /**
     * @ORM\Column(type="datetime")
     * @Serializer\Groups({"userDetails"})
     */
    protected $rfidExpireDate;

    /**
     * @ORM\Column(type="datetime")
     * @Serializer\Groups({"userDetails"})
     */
    protected $memberSince;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rfid
     *
     * @param integer $rfid
     * @return User
     */
    public function setRfid($rfid)
    {
        $this->rfid = $rfid;

        return $this;
    }

    /**
     * Get rfid
     *
     * @return integer 
     */
    public function getRfid()
    {
        return $this->rfid;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set dateOfBirth
     *
     * @param \DateTime $dateOfBirth
     * @return User
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * Get dateOfBirth
     *
     * @return \DateTime 
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return User
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     * @return User
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return string 
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set imageUrl
     *
     * @param string $imageUrl
     * @return User
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    /**
     * Get imageUrl
     *
     * @return string 
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * Set rfidExpireDate
     *
     * @param \DateTime $rfidExpireDate
     * @return User
     */
    public function setRfidExpireDate($rfidExpireDate)
    {
        $this->rfidExpireDate = $rfidExpireDate;

        return $this;
    }

    /**
     * Get rfidExpireDate
     *
     * @return \DateTime 
     */
    public function getRfidExpireDate()
    {
        return $this->rfidExpireDate;
    }

    /**
     * Set memberSince
     *
     * @param \DateTime $memberSince
     * @return User
     */
    public function setMemberSince($memberSince)
    {
        $this->memberSince = $memberSince;

        return $this;
    }

    /**
     * Get memberSince
     *
     * @return \DateTime 
     */
    public function getMemberSince()
    {
        return $this->memberSince;
    }

}

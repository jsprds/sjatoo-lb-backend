<?php

namespace Jds\ApiBundle\Entity;

use Jds\ApiBundle\Model\EventInterface;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity()
 * @ORM\Table(name="event")
 */
class Event implements EventInterface
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer", length=6)
	 * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"eventList", "orderDetails"})
	 */
    protected $id;

    /**
     * @ORM\Column(type="string", length=140)
     * @Serializer\Groups({"eventList", "userList"})
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     * @Serializer\Groups({"eventList", "userList"})
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime")
     * @Serializer\Groups({"eventList", "userList"})
     */
    private $endDate;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2)
     */
    private $memberPrice;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=140)
     * @Serializer\Groups({"eventList", "userList"})
     */
    private $facebookUrl;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Event
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return Event
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return Event
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set memberPrice
     *
     * @param string $memberPrice
     * @return Event
     */
    public function setMemberPrice($memberPrice)
    {
        $this->memberPrice = $memberPrice;

        return $this;
    }

    /**
     * Get memberPrice
     *
     * @return string 
     */
    public function getMemberPrice()
    {
        return $this->memberPrice;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Event
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set facebookUrl
     *
     * @param string $facebookUrl
     * @return Event
     */
    public function setFacebookUrl($facebookUrl)
    {
        $this->facebookUrl = $facebookUrl;

        return $this;
    }

    /**
     * Get facebookUrl
     *
     * @return string 
     */
    public function getFacebookUrl()
    {
        return $this->facebookUrl;
    }
}

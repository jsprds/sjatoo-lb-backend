<?php

namespace Jds\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hier wordt een prijs gekoppeld aan een product a.d.h.v. een datum. 
 * 
 * @ORM\Entity()
 * @ORM\Table(name="product_price")
 */
class ProductPrice
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer", length=10)
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Price")
     */
    protected $price;

    /**
     * StartDatum verplicht
     *
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $startDate;

    /**
     * Indien geen einddatum --> Startdatum tot heden
     * 
     * @ORM\Column(type="datetime")
     */
    protected $endDate;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return ProductPrice
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return ProductPrice
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set price
     *
     * @param \Jds\ApiBundle\Entity\Price $price
     * @return ProductPrice
     */
    public function setPrice(\Jds\ApiBundle\Entity\Price $price = null)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return \Jds\ApiBundle\Entity\Price 
     */
    public function getPrice()
    {
        return $this->price;
    }
}

<?php

namespace Jds\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Jds\ApiBundle\Model\ProductInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity()
 */
class Product implements ProductInterface
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer", length=3)
	 * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"productList", "productDetails", "orderList", "orderDetails"})
	 */
    protected $id;

    /**
	 * @ORM\Column(type="string", length=70)
     * @Serializer\Groups({"productList", "productDetails", "orderList", "orderDetails"})
	 */
    protected $name;


  	/**
     * @ORM\Column(type="decimal", precision=4, scale=2)
     * @Serializer\Groups({"productList", "productDetails"})
     */
    protected $price;

    /**
	 * @ORM\Column(type="string", length=140)
     * @Serializer\Groups({"productDetails"})
	 */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="ProductCategory")
     * @Serializer\Groups({"productList", "productDetails"})
     */
    protected $category;

    /**
     * Volledige URL te beginnen met http://voorbeeld.be/afbeelding.jpg (shortner gebruiken als url langer is als 140 tekens)
     *
	 * @ORM\Column(type="string", length=140)
     * @Serializer\Groups({"productDetails"})
	 */
    protected $image;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Product
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set category
     *
     * @param \Jds\ApiBundle\Entity\ProductCategory $category
     * @return Product
     */
    public function setCategory(\Jds\ApiBundle\Entity\ProductCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Jds\ApiBundle\Entity\ProductCategory 
     */
    public function getCategory()
    {
        return $this->category;
    }
}

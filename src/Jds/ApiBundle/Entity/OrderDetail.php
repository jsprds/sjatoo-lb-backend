<?php

namespace Jds\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;


/**
 * @ORM\Entity()
 * @ORM\Table(name="order_detail")
 */
class OrderDetail
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer", length=8)
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Product")
     * @Serializer\Groups({"orderList", "orderDetails"})
     */
    protected $product;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2)
     * @Serializer\Groups({"orderList", "orderDetails"})
     */
    protected $price;

	/**
     * @ORM\Column(type="integer", length=2)
     * @Serializer\Groups({"orderList", "orderDetails"})
     */
    protected $amount;

    /**
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="orderDetails", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     **/
    protected $order;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return OrderDetail
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return OrderDetail
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set product
     *
     * @param \Jds\ApiBundle\Entity\Product $product
     * @return OrderDetail
     */
    public function setProduct(\Jds\ApiBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Jds\ApiBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set order
     *
     * @param \Jds\ApiBundle\Entity\Order $order
     * @return OrderDetail
     */
    public function setOrder(\Jds\ApiBundle\Entity\Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \Jds\ApiBundle\Entity\Order 
     */
    public function getOrder()
    {
        return $this->order;
    }
}

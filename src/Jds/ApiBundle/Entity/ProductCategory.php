<?php

namespace Jds\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity()
 * @ORM\Table(name="product_category")
 */
class ProductCategory
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer", length=3)
	 * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"productList", "productDetails", "productCategoryList"})
	 */
    protected $id;

    /**
	 * @ORM\Column(type="string", length=70)
     * @Serializer\Groups({"productList", "productDetails", "productCategoryList"})
	 */
    protected $name;

    /**
	 * @ORM\Column(type="string", length=140)
	 */
    protected $description;

    /**
     * @ORM\Column(type="string", length=10)
     * @Serializer\Groups({"productList", "productDetails", "productCategoryList"})
     */
    protected $abbreviation;

    /**
     * Volledige URL te beginnen met http://voorbeeld.be/afbeelding.jpg (shortner gebruiken als url langer is als 140 tekens)
     *
	 * @ORM\Column(type="string", length=140)
	 */
    protected $image;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProductCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ProductCategory
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set abbreviation
     *
     * @param string $abbreviation
     * @return ProductCategory
     */
    public function setAbbreviation($abbreviation)
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    /**
     * Get abbreviation
     *
     * @return string 
     */
    public function getAbbreviation()
    {
        return $this->abbreviation;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return ProductCategory
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }
}

<?php

namespace Jds\ApiBundle\Entity;

use Jds\ApiBundle\Model\OrderInterface;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity()
 * @ORM\Table(name="the_order")
 */
class Order implements OrderInterface
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer", length=6)
	 * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"orderList", "orderDetails"})
	 */
    protected $id;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @Serializer\Groups({"orderList", "orderDetails"})
     */
    protected $orderDate;

    //protected $orderedBy;

    //protected $handledBy;

    /**
     * @ORM\OneToMany(targetEntity="OrderDetail", mappedBy="order", cascade={"persist", "remove"})
     * @Serializer\Groups({"orderList", "orderDetails"})
     **/
    protected $orderDetails;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @Serializer\Groups({"orderList", "orderDetails"})
     */
    protected $status = false;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orderDetails = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add orderDetails
     *
     * @param \Jds\ApiBundle\Entity\OrderDetail $orderDetails
     * @return Order
     */
    public function addOrderDetail(\Jds\ApiBundle\Entity\OrderDetail $orderDetail)
    {
        $orderDetail->setOrder($this);// Assign this order to $orderDetail object
        $this->orderDetails->add($orderDetail);
        return $this;
    }

    /**
     * Remove orderDetails
     *
     * @param \Jds\ApiBundle\Entity\OrderDetail $orderDetails
     */
    public function removeOrderDetail(\Jds\ApiBundle\Entity\OrderDetail $orderDetails)
    {
        $this->orderDetails->removeElement($orderDetails);
    }

    /**
     * Get orderDetails
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrderDetails()
    {
        return $this->orderDetails;
    }

    /**
     * Set orderDate
     *
     * @param \DateTime $orderDate
     * @return Order
     */
    public function setOrderDate($orderDate)
    {
        $this->orderDate = $orderDate;

        return $this;
    }

    /**
     * Get orderDate
     *
     * @return \DateTime 
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Order
     */
    public function setStatus($status)
    {     
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }
}

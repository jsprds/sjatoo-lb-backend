<?php

namespace Jds\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Util\Codes;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Jds\ApiBundle\Model\ProductCategoryInterface;

class ProductCategoryController extends FOSRestController
{
    /**
     * List all product categories.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing product categories.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="25", description="How many product categories to return.")
     * @Annotations\QueryParam(name="order_by", requirements="\d+", default="id", description="How many product categories to return.")
     *
     * @Annotations\View(
     *      serializerGroups={"productCategoryList"}
     * )
     *
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getProductcategoriesAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');
        $orderBy = $paramFetcher->get('order_by');

        return $this->container->get('jds_api.productCategory.handler')->all($limit, $offset, $orderBy);
    }

    public function optionsProductCategorysAction() {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, PATCH, POST, PUT');
        return $response;
    }
}
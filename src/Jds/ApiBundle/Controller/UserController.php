<?php

namespace Jds\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\Response;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use FOS\RestBundle\Controller\Annotations\NamePrefix;       // NamePrefix Route annotation class @NamePrefix("bdk_core_user_userrest_")
use FOS\RestBundle\View\RouteRedirectView;                  // Route based redirect implementation
use FOS\RestBundle\View\View AS FOSView;                    // Default View implementation.
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\ConstraintViolation;
use JMS\SecurityExtraBundle\Annotation\Secure;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\RequestParam;

class UserController extends FOSRestController
{
    /**
     * List all users.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing users.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="25", description="How many users to return.")
     * @Annotations\QueryParam(name="dateOfBirth", description="Search by dateOfBirth")
     *
     * @Annotations\View(
     *      serializerGroups={"userList"}
     * )
     *
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getUsersAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');
        $search = [];

        if($paramFetcher->get('dateOfBirth')) {
            $search['dateOfBirth'] = new \DateTime($paramFetcher->get('dateOfBirth'));
        }

        return $this->container->get('jds_api.user.handler')->all($limit, $offset, $search);
    }

    /**
     * Get single User.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Gets a user for a given id",
     *   output = "Jds\ApiBundle\Entity\User",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the page is not found"
     *   }
     * )
     *
     * @Annotations\View(
     *    serializerGroups={"userDetails"}
     * )
     *
     * @param int     $id      the group id
     *
     * @return array
     *
     * @throws NotFoundHttpException when group not exist
     */
    public function getUserAction($id)
    {
        var_dump('get user');
        return $this->getOr404($id);
    }

    /**
     * Create a user from the submitted data.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a new user from the submitted data.",
     *   input = "Jds\MemberBundle\Form\PageType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Annotations\View(
     *  template = "JdsMemberBundle:Group:newGroup.html.twig",
     *  statusCode = Codes::HTTP_BAD_REQUEST,
     *  templateVar = "form"
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postUserAction(Request $request)
    {   
        $request = $this->getRequest();

        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->createUser();
       
        $user->setRfid($request->get('rfid'));
        $user->setFirstname($request->get('firstname'));
        $user->setLastname($request->get('lastname'));
        $user->setStreet($request->get('street'));
        $user->setZipcode($request->get('zipcode'));
        $user->setCity($request->get('city'));
        $user->setGender($request->get('gender'));
        $user->setMemberSince(new \DateTime());
        if($request->get('dateOfBirth')) {
            $user->setDateOfBirth(new \DateTime($request->get('dateOfBirth')));
        }
        if($request->get('rfidExpireDate')) {
            $date = new \DateTime($request->get('rfidExpireDate'));
            $date->setTimezone(new \DateTimeZone('Europe/Brussels'));
            $user->setRfidExpireDate($date);
        }
        
        $uploader = new \Jds\ApiBundle\Helper\ImageUploader('uploads/member_pictures/');
        $user->setImageUrl($uploader->base64Image($request->get('image')));

        $validator = $this->get('validator');
        $errors = $validator->validate($user, array('rfid_registration'));

        if(count($errors) == 0) {
            $userManager->updateUser($user);

            $routeOptions = array(
                'id' => $user->getId(),
                '_format' => $request->get('_format')
            );

            $view = $this->view($user, 201);
            return $this->handleView($view);

            //return $this->routeRedirectView('get_user', $routeOptions, Codes::HTTP_CREATED);

        } 
        else {
            return $errors;
        }
    }

    public function optionsUsersAction() {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, PATCH, POST, PUT');
        return $response;
    }

    /**
     * Fetch a User or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return UserInterface
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($id)
    {
        if (!($user = $this->container->get('jds_api.user.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('The user \'%s\' was not found.',$id));
        }

        return $user;
    }
}
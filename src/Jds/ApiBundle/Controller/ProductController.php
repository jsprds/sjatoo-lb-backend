<?php

namespace Jds\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Util\Codes;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class ProductController extends FOSRestController
{
    /**
     * List all products.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing products.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="25", description="How many products to return.")
     *
     * @Annotations\View(
     *      serializerGroups={"productList"}
     * )
     *
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getProductsAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');

        return $this->container->get('jds_api.product.handler')->all($limit, $offset);
    }

    /**
     * Get single Product.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Gets a product for a given id",
     *   output = "Jds\ApiBundle\Entity\Product",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the page is not found"
     *   }
     * )
     *
     * @Annotations\View(
     *    serializerGroups={"productDetails"}
     * )
     *
     * @param int     $id      the group id
     *
     * @return array
     *
     * @throws NotFoundHttpException when group not exist
     */
    public function getProductAction($id)
    {
        return $this->getOr404($id);
    }

    /**
     * Create a user from the submitted data.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a new user from the submitted data.",
     *   input = "Jds\MemberBundle\Form\PageType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Annotations\View(
     *  template = "JdsMemberBundle:Group:newGroup.html.twig",
     *  statusCode = Codes::HTTP_BAD_REQUEST,
     *  templateVar = "form"
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postUserAction(Request $request)
    {   
        // code van: http://stackoverflow.com/questions/10606792/fosuserbundle-integration-with-fosrestbundle
        $form = $this->container->get('fos_user.registration.form');
        $formHandler = $this->container->get('fos_user.registration.form.handler');
        $confirmationEnabled = $this->container->getParameter('fos_user.registration.confirmation.enabled');

        $process = $formHandler->process($confirmationEnabled);

        if ($process) {
            $user = $form->getData();
            $authUser = false;

            if ($confirmationEnabled) {
            } else {
                $authUser = true;
            }

            $response = new Response();

            if ($authUser) {
                /* @todo Implement authentication */
                //$this->authenticateUser($user, $response);
            }

            $response->setStatusCode(Codes::HTTP_CREATED);
            $response->headers->set(
                'Location',
                $this->generateUrl(
                    'api_users_get_user',
                    array('user' => $user->getId()),
                    true
                )
            );

            return $response;
        }

        return RestView::create($form, Codes::HTTP_BAD_REQUEST);


        // try {

        //     $newUser = $this->container->get('jds_api.user.handler')->post(
        //         $request->request->all()
        //     );

        //     $routeOptions = array(
        //         'id' => $newUser->getId(),
        //         '_format' => $request->get('_format')
        //     );

        //     return $this->routeRedirectView('get_user', $routeOptions, Codes::HTTP_CREATED);

        // } catch (InvalidFormException $exception) {

        //     return $exception->getForm();
        // }
    }
}
<?php

namespace Jds\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Util\Codes;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Jds\ApiBundle\Model\OrderInterface;

class OrderController extends FOSRestController
{
    /**
     * List all orders.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing orders.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="25", description="How many orders to return.")
     * @Annotations\QueryParam(name="order_by", requirements="", default="orderDate", description="How many orders to return.")
     * @Annotations\QueryParam(name="status", requirements="", default="false", description="How many orders to return.")
     *
     * @Annotations\View(
     *      serializerGroups={"orderList"}
     * )
     *
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getOrdersAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');
        $orderBy = $paramFetcher->get('order_by');

        $criteria['status'] = $paramFetcher->get('status');

        return $this->container->get('jds_api.order.handler')->all($limit, $offset, $orderBy, $criteria);
    }

    /**
     * Get single Order.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Gets a order for a given id",
     *   output = "Jds\ApiBundle\Entity\Order",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the order is not found"
     *   }
     * )
     *
     * @Annotations\View(
     *    serializerGroups={"orderDetails"}
     * )
     *
     * @param int     $id      the order id
     *
     * @return array
     *
     * @throws NotFoundHttpException when order not exist
     */
    public function getOrderAction($id)
    {
        return $this->getOr404($id);
    }

    /**
     * Create an order  from the submitted data.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a new order from the submitted data.",
     *   input = "Jds\MemberBundle\Form\PageType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Annotations\View(
     *  template = "JdsMemberBundle:Group:newGroup.html.twig",
     *  statusCode = Codes::HTTP_BAD_REQUEST,
     *  templateVar = "form"
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postOrderAction(Request $request)
    {   
        try {

            $newOrder = $this->container->get('jds_api.order.handler')->post(
                $request->request->all()
            );

            $routeOptions = array(
                'id' => $newOrder->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('get_order', $routeOptions, Codes::HTTP_CREATED);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    /**
     * Update an order from the submitted data.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Update an order from the submitted data.",
     *   input = "Jds\MemberBundle\Form\PageType",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Annotations\View(
     *  statusCode = Codes::HTTP_BAD_REQUEST,
     *  templateVar = "form"
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function putOrderAction($id, Request $request)
    {
        try {
            if (!($order = $this->container->get('jds_api.order.handler')->get($id))) {
                $statusCode = Codes::HTTP_CREATED;
                $order = $this->container->get('jds_api.order.handler')->post(
                    $request->request->all()
                );
            } else {
                $statusCode = Codes::HTTP_NO_CONTENT;
                $order = $this->container->get('jds_api.order.handler')->put(
                    $order,
                    $request->request->all()
                );
            }

            $routeOptions = array(
                'id' => $order->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('get_order', $routeOptions, $statusCode);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    /**
     * Partially update an Order.
     *
     * @param OrderInterface $page
     * @param array         $parameters
     *
     * @return OrderInterface
     */
    public function patchOrderAction(Request $request, $id)
    {
        try {
            $order = $this->container->get('jds_api.order.handler')->patch(
                $this->getOr404($id),
                $request->request->all()
            );

            $routeOptions = array(
                'id' => $order->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('get_order', $routeOptions, Codes::HTTP_NO_CONTENT);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    /**
     * Deteletes an Order.
     *
     * @param OrderInterface $order
     * @param array         $parameters
     *
     * @return OrderInterface
     */
    public function deleteOrderAction(Request $request, $id)
    {
        try {
            $order = $this->container->get('jds_api.order.handler')->patch(
                $this->getOr404($id),
                $request->request->all()
            );

            $routeOptions = array(
                'id' => $order->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('get_order', $routeOptions, Codes::HTTP_NO_CONTENT);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    public function optionsOrdersAction() {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, PATCH, POST, PUT');
        return $response;
    }

    /**
     * Fetch a Order or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return OrderInterface
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($id)
    {
        if (!($group = $this->container->get('jds_api.order.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('The order \'%s\' was not found.',$id));
        }

        return $group;
    }
}
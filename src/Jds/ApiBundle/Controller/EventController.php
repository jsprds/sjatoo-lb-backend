<?php

namespace Jds\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Util\Codes;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Jds\ApiBundle\Model\EventInterface;

class EventController extends FOSRestController
{
    /**
     * List all events.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset from which to start listing events.")
     * @Annotations\QueryParam(name="limit", requirements="\d+", default="25", description="How many events to return.")
     * @Annotations\QueryParam(name="order_by", requirements="", default="id", description="How many events to return.")
     * @Annotations\QueryParam(name="date", nullable=true,  description="search on date")
     *
     * @Annotations\View(
     *      serializerGroups={"eventList"}
     * )
     *
     *
     * @param Request               $request      the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getEventsAction(Request $request, ParamFetcherInterface $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');
        $orderBy = $paramFetcher->get('order_by');

        $criteria = array();

        if($paramFetcher->get('date')) {
            $criteria['date'] = new \DateTime($paramFetcher->get('date'));
            $criteria['date']->setTimezone(new \DateTimeZone('Europe/Brussels'));
        }
        
        return $this->container->get('jds_api.event.handler')->all($limit, $offset, $orderBy, $criteria);
    }

    /**
     * Get single Event.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Gets a event for a given id",
     *   output = "Jds\ApiBundle\Entity\Event",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the event is not found"
     *   }
     * )
     *
     * @Annotations\View(
     *    serializerGroups={"eventDetails"}
     * )
     *
     * @param int     $id      the event id
     *
     * @return array
     *
     * @throws NotFoundHttpException when event not exist
     */
    public function getEventAction($id)
    {
        return $this->getOr404($id);
    }

    /**
     * Create an event  from the submitted data.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a new event from the submitted data.",
     *   input = "Jds\MemberBundle\Form\PageType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Annotations\View(
     *  template = "JdsMemberBundle:Group:newGroup.html.twig",
     *  statusCode = Codes::HTTP_BAD_REQUEST,
     *  templateVar = "form"
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postEventAction(Request $request)
    {   
        try {

            $newEvent = $this->container->get('jds_api.event.handler')->post(
                $request->request->all()
            );

            $routeOptions = array(
                'id' => $newEvent->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('get_event', $routeOptions, Codes::HTTP_CREATED);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    /**
     * Update an event from the submitted data.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Update an event from the submitted data.",
     *   input = "Jds\MemberBundle\Form\PageType",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Annotations\View(
     *  statusCode = Codes::HTTP_BAD_REQUEST,
     *  templateVar = "form"
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function putEventAction($id, Request $request)
    {
        try {
            if (!($event = $this->container->get('jds_api.event.handler')->get($id))) {
                $statusCode = Codes::HTTP_CREATED;
                $event = $this->container->get('jds_api.event.handler')->post(
                    $request->request->all()
                );
            } else {
                $statusCode = Codes::HTTP_NO_CONTENT;
                $event = $this->container->get('jds_api.event.handler')->put(
                    $event,
                    $request->request->all()
                );
            }

            $routeOptions = array(
                'id' => $event->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('get_event', $routeOptions, $statusCode);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    /**
     * Partially update an Event.
     *
     * @param EventInterface $page
     * @param array         $parameters
     *
     * @return EventInterface
     */
    public function patchEventAction(Request $request, $id)
    {
        try {
            $event = $this->container->get('jds_api.event.handler')->patch(
                $this->getOr404($id),
                $request->request->all()
            );

            $routeOptions = array(
                'id' => $event->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('get_event', $routeOptions, Codes::HTTP_NO_CONTENT);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    /**
     * Deteletes an Event.
     *
     * @param EventInterface $event
     * @param array         $parameters
     *
     * @return EventInterface
     */
    public function deleteEventAction(Request $request, $id)
    {
        try {
            $event = $this->container->get('jds_api.event.handler')->patch(
                $this->getOr404($id),
                $request->request->all()
            );

            $routeOptions = array(
                'id' => $event->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('get_event', $routeOptions, Codes::HTTP_NO_CONTENT);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    public function optionsEventsAction() {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, PATCH, POST, PUT');
        return $response;
    }

    /**
     * Fetch a Event or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return EventInterface
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($id)
    {
        if (!($group = $this->container->get('jds_api.event.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('The event \'%s\' was not found.',$id));
        }

        return $group;
    }
}
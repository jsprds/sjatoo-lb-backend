<?php

namespace Jds\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Util\Codes;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Jds\ApiBundle\Model\CheckinInterface;

use Symfony\Component\HttpKernel\Exception\HttpException;

class CheckinController extends FOSRestController
{
    /**
     * Get single Checkin.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Gets a checkin for a given id",
     *   output = "Jds\ApiBundle\Entity\Checkin",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the checkin is not found"
     *   }
     * )
     *
     * @Annotations\View(
     *    serializerGroups={"checkinDetails"}
     * )
     *
     * @param int     $id      the checkin id
     *
     * @return array
     *
     * @throws NotFoundHttpException when checkin not exist
     */
    public function getCheckinAction($id)
    {
        return $this->getOr404($id);
    }

    /**
     * Create an checkin  from the submitted data.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Creates a new checkin from the submitted data.",
     *   input = "Jds\MemberBundle\Form\PageType",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Annotations\View(
     *  template = "JdsMemberBundle:Group:newGroup.html.twig",
     *  statusCode = Codes::HTTP_BAD_REQUEST,
     *  templateVar = "form"
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postCheckinAction(Request $request)
    {   
        try {

            // eerst checken of de user al niet perongeluk net ingecheckt is
            $results = $this->container->get('jds_api.checkin.handler')->all(
                $limit = 1, $offset = 0, 
                $orderBy = '-checkinDate', 
                $criteria = array(
                    'user' => $request->request->all()['user']
                )
            );

            if(count($results)) {
               $lastCheckin = $results[0];
               $dateTimeInterval = (new \DateTime())->diff($lastCheckin->getCheckinDate());
               
                var_dump($dateTimeInterval->i);

               // Als er minder als 4 minuten verstreken zijn --> error
               if($dateTimeInterval->i < 4) {
                    throw new HttpException(400, "Deze kaart is minder als 4 minuten geleden al ingecheckt");
               }
            }

            $newCheckin = $this->container->get('jds_api.checkin.handler')->post(
                $request->request->all()
            );

            $routeOptions = array(
                'id' => $newCheckin->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('get_checkin', $routeOptions, Codes::HTTP_CREATED);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    /**
     * Update an checkin from the submitted data.
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Update an checkin from the submitted data.",
     *   input = "Jds\MemberBundle\Form\PageType",
     *   statusCodes = {
     *     204 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @Annotations\View(
     *  statusCode = Codes::HTTP_BAD_REQUEST,
     *  templateVar = "form"
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function putCheckinAction($id, Request $request)
    {
        try {
            if (!($checkin = $this->container->get('jds_api.checkin.handler')->get($id))) {
                $statusCode = Codes::HTTP_CREATED;
                $checkin = $this->container->get('jds_api.checkin.handler')->post(
                    $request->request->all()
                );
            } else {
                $statusCode = Codes::HTTP_NO_CONTENT;
                $checkin = $this->container->get('jds_api.checkin.handler')->put(
                    $checkin,
                    $request->request->all()
                );
            }

            $routeOptions = array(
                'id' => $checkin->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('get_checkin', $routeOptions, $statusCode);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    /**
     * Partially update an Checkin.
     *
     * @param CheckinInterface $page
     * @param array         $parameters
     *
     * @return CheckinInterface
     */
    public function patchCheckinAction(Request $request, $id)
    {
        try {
            $checkin = $this->container->get('jds_api.checkin.handler')->patch(
                $this->getOr404($id),
                $request->request->all()
            );

            $routeOptions = array(
                'id' => $checkin->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('get_checkin', $routeOptions, Codes::HTTP_NO_CONTENT);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    /**
     * Deteletes an Checkin.
     *
     * @param CheckinInterface $checkin
     * @param array         $parameters
     *
     * @return CheckinInterface
     */
    public function deleteCheckinAction(Request $request, $id)
    {
        try {
            $checkin = $this->container->get('jds_api.checkin.handler')->patch(
                $this->getOr404($id),
                $request->request->all()
            );

            $routeOptions = array(
                'id' => $checkin->getId(),
                '_format' => $request->get('_format')
            );

            return $this->routeRedirectView('get_checkin', $routeOptions, Codes::HTTP_NO_CONTENT);

        } catch (InvalidFormException $exception) {

            return $exception->getForm();
        }
    }

    public function optionsCheckinAction() {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, PATCH, POST, PUT');
        return $response;
    }

    /**
     * Fetch a Checkin or throw an 404 Exception.
     *
     * @param mixed $id
     *
     * @return CheckinInterface
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($id)
    {
        if (!($group = $this->container->get('jds_api.checkin.handler')->get($id))) {
            throw new NotFoundHttpException(sprintf('The checkin \'%s\' was not found.',$id));
        }

        return $group;
    }
}
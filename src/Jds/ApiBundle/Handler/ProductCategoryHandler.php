<?php

namespace Jds\ApiBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use Jds\ApiBundle\Model\ProductCategoryInterface;
use Jds\ApiBundle\Exception\InvalidFormException;

class ProductCategoryHandler implements ProductCategoryHandlerInterface
{
    private $om;
    private $entityClass;
    private $repository;
    private $formFactory;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory)
    {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
    }

    /**
     * Get a list of ProductCategorys.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 25, $offset = 0, $orderBy = 'pc.id')
    {
       /// Start query
        $qb = $this->om->getRepository($this->entityClass)->createQueryBuilder('pc');

        /* SET ORDER BY */
        if($orderBy != null) {
            $sort = (strpos($orderBy, '-') === 0 ? 'DESC' : 'ASC');
            $orderBy = 'pc.' . ltrim($orderBy, '-');
            $qb->orderBy($orderBy, $sort);
        }

        $qb->setFirstResult($offset);
        $qb->setMaxResults($limit);

        $query =  $qb->getQuery();

        return $query->execute();
    }

    private function createProductCategory()
    {
        return new $this->entityClass();
    }
}
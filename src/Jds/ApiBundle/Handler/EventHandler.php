<?php

namespace Jds\ApiBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use Jds\ApiBundle\Model\EventInterface;
use Jds\ApiBundle\Form\Type\EventType;
use Jds\ApiBundle\Exception\InvalidFormException;

class EventHandler implements EventHandlerInterface
{
    private $om;
    private $entityClass;
    private $repository;
    private $formFactory;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory)
    {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
    }

    /**
     * Get a Event.
     *
     * @param mixed $id
     *
     * @return EventInterface
     */
    public function get($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Get a list of Events.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 25, $offset = 0, $orderBy = 'e.id', $criteria = array())
    {
        /// Start query
        $qb = $this->om->getRepository($this->entityClass)->createQueryBuilder('e');

        // criteria
        if(count($criteria) > 0) {
            if($criteria['date']) {
                $qb->andWhere("e.startDate < :eventDate")
                   ->andWhere("e.endDate > :eventDate")
                   ->setParameter("eventDate", $criteria['date']->format('Y-m-d H:i:s'));
            }
        }

        /* SET ORDER BY */
        if($orderBy != null) {
            $sort = (strpos($orderBy, '-') === 0 ? 'DESC' : 'ASC');
            $orderBy = 'e.' . ltrim($orderBy, '-');
            $qb->orderBy($orderBy, $sort);
        }

        $qb->setFirstResult($offset);
        $qb->setMaxResults($limit);

        $query =  $qb->getQuery();

        return $query->execute();
    }

    /**
     * Create a new Event.
     *
     * @param array $parameters
     *
     * @return EventInterface
     */
    public function post(array $parameters)
    {
        $order = $this->createEvent();

        return $this->processForm($order, $parameters, 'POST');
    }

    /**
     * Partially update a Event.
     *
     * @param EventInterface $order
     * @param array         $parameters
     *
     * @return EventInterface
     */
    public function patch(EventInterface $event, array $parameters)
    {
        return $this->processForm($event, $parameters, 'PATCH');
    }

    /**
     * Edit a Event.
     *
     * @param EventInterface    $event
     * @param array             $parameters
     *
     * @return EventInterface
     */
    public function put(EventInterface $event, array $parameters)
    {
        return $this->processForm($event, $parameters, 'PUT');
    }

    /**
     * Processes the form.
     *
     * @param EventInterface    $event
     * @param array             $parameters
     * @param String            $method
     *
     * @return EventInterface
     *
     * @throws \Jds\EventBundle\Exception\InvalidFormException
     */
    private function processForm(EventInterface $event, array $parameters, $method = "PUT")
    {
        $form = $this->formFactory->create(new EventType(), $event, array('method' => $method));
        $form->submit($parameters, 'PATCH' !== $method);

        if ($form->isValid()) {
            
            $event = $form->getData();
            $this->om->persist($event);
            $this->om->persist($event);
            $this->om->flush($event);

            return $event;
        }

        // debug form
        //var_dump($form->getErrorsAsString());

        throw new InvalidFormException('Invalid submitted data: ' . $form->getErrorsAsString(), $form);
    }

    private function createEvent()
    {
        return new $this->entityClass();
    }
}
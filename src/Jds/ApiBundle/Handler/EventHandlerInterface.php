<?php

namespace Jds\ApiBundle\Handler;

use Jds\ApiBundle\Model\EventInterface;

interface EventHandlerInterface
{
    /**
     * Get a Event given the identifier
     *
     * @api
     *
     * @param mixed $id
     *
     * @return EventInterface
     */
    public function get($id);

    /**
     * Get a list of orders.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 5, $offset = 0, $orderby = null);

    /**
     * Post Event, creates a new Event.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return EventInterface
     */
    public function post(array $parameters);

    /**
     * Edit a Event.
     *
     * @api
     *
     * @param EventInterface   $product
     * @param array           $parameters
     *
     * @return EventInterface
     */
    public function put(EventInterface $product, array $parameters);

    /**
     * Partially update a Event.
     *
     * @api
     *
     * @param EventInterface   $product
     * @param array           $parameters
     *
     * @return EventInterface
     */
    public function patch(EventInterface $product, array $parameters);
}
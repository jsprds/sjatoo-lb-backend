<?php

namespace Jds\ApiBundle\Handler;

use Jds\ApiBundle\Model\ProductCategoryInterface;

interface ProductCategoryHandlerInterface
{
    /**
     * Get a list of products.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 5, $offset = 0, $orderby = null);
}
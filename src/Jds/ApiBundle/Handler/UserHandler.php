<?php

namespace Jds\ApiBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use Jds\ApiBundle\Model\UserInterface;
use Jds\ApiBundle\Form\Type\UserType;
use Jds\ApiBundle\Exception\InvalidFormException;

class UserHandler implements UserHandlerInterface
{
    private $om;
    private $entityClass;
    private $repository;
    private $formFactory;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory)
    {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
    }

    /**
     * Get a User.
     *
     * @param mixed $id
     *
     * @return UserInterface
     */
    public function get($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Get a User.
     *
     * @param mixed $rfid
     *
     * @return UserInterface
     */
    public function getByRfid($rfid)
    {
        return $this->repository->findOneBy(array('rfid' => $rfid));
    }

    /**
     * Get a list of Users.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 25, $offset = 0, $search = array(), $orderBy = 'p.id')
    {
       /// Start query
        $qb = $this->om->getRepository($this->entityClass)->createQueryBuilder('p');

        /* SET ORDER BY */
        if($orderBy != null) {
            $sort = (strpos($orderBy, '-') === 0 ? 'DESC' : 'ASC');
            $orderBy = ltrim($orderBy, '-');
            $qb->orderBy($orderBy, $sort);
        }

        if(count($search) > 0) {
            if(isset($search['dateOfBirth'])) {
                $qb->andWhere("p.dateOfBirth = :date")
                   ->setParameter("date", $search['dateOfBirth']->format('Y-m-d'));
            }
        }

        $qb->setFirstResult($offset);
        $qb->setMaxResults($limit);

        $query =  $qb->getQuery();

        return $query->execute();
    }

    /**
     * Create a new User.
     *
     * @param array $parameters
     *
     * @return UserInterface
     */
    public function post(array $parameters)
    {
        $user = $this->createUser();

        return $this->processForm($user, $parameters, 'POST');
    }

    /**
     * Edit a User.
     *
     * @param UserInterface $user
     * @param array         $parameters
     *
     * @return UserInterface
     */
    public function put(UserInterface $user, array $parameters)
    {
        return $this->processForm($user, $parameters, 'PUT');
    }

    /**
     * Partially update a User.
     *
     * @param UserInterface $user
     * @param array         $parameters
     *
     * @return UserInterface
     */
    public function patch(UserInterface $user, array $parameters)
    {
        return $this->processForm($user, $parameters, 'PATCH');
    }

    /**
     * Processes the form.
     *
     * @param UserInterface $user
     * @param array         $parameters
     * @param String        $method
     *
     * @return UserInterface
     *
     * @throws \Jds\UserBundle\Exception\InvalidFormException
     */
    private function processForm(UserInterface $user, array $parameters, $method = "PUT")
    {
        $form = $this->formFactory->create(new UserType(), $user, array('method' => $method));
        
        $form->submit($parameters, 'PATCH' !== $method);
        if ($form->isValid()) {
            
            $user = $form->getData();
            
            var_dump($form->getData());

            //$this->om->persist($user);
            //$this->om->flush($user);

            return $user;
        }

        // debug form
        var_dump($form->getErrorsAsString());

        throw new InvalidFormException('Invalid submitted data', $form);
    }

    private function createUser()
    {
        return new $this->entityClass();
    }
}
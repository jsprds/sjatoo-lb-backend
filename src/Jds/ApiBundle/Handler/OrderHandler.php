<?php

namespace Jds\ApiBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use Jds\ApiBundle\Model\OrderInterface;
use Jds\ApiBundle\Form\Type\OrderType;
use Jds\ApiBundle\Exception\InvalidFormException;

class OrderHandler implements OrderHandlerInterface
{
    private $om;
    private $entityClass;
    private $repository;
    private $formFactory;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory)
    {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
    }

    /**
     * Get a Order.
     *
     * @param mixed $id
     *
     * @return OrderInterface
     */
    public function get($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Get a list of Orders.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 25, $offset = 0, $orderBy = 't.id', $criteria = array())
    {
        /// Start query
        $qb = $this->om->getRepository($this->entityClass)->createQueryBuilder('t');

        // criteria
        if(count($criteria) > 0) {
            if($criteria['status']) {
                $qb->andWhere("t.status = :status")
                   ->setParameter("status", $criteria['status']);
            }
        }

        /* SET ORDER BY */
        if($orderBy != null) {
            $sort = (strpos($orderBy, '-') === 0 ? 'DESC' : 'ASC');
            $orderBy = 't.' . ltrim($orderBy, '-');
            $qb->orderBy($orderBy, $sort);
        }

        $qb->setFirstResult($offset);
        $qb->setMaxResults($limit);

        $query =  $qb->getQuery();

        return $query->execute();
    }

    /**
     * Create a new Order.
     *
     * @param array $parameters
     *
     * @return OrderInterface
     */
    public function post(array $parameters)
    {
        $order = $this->createOrder();

        return $this->processForm($order, $parameters, 'POST');
    }

    /**
     * Partially update a Order.
     *
     * @param OrderInterface $order
     * @param array         $parameters
     *
     * @return OrderInterface
     */
    public function patch(OrderInterface $order, array $parameters)
    {
        return $this->processForm($order, $parameters, 'PATCH');
    }

    /**
     * Edit a Order.
     *
     * @param OrderInterface    $order
     * @param array             $parameters
     *
     * @return OrderInterface
     */
    public function put(OrderInterface $order, array $parameters)
    {
        return $this->processForm($order, $parameters, 'PUT');
    }

    /**
     * Processes the form.
     *
     * @param OrderInterface    $order
     * @param array             $parameters
     * @param String            $method
     *
     * @return OrderInterface
     *
     * @throws \Jds\OrderBundle\Exception\InvalidFormException
     */
    private function processForm(OrderInterface $order, array $parameters, $method = "PUT")
    {
        $form = $this->formFactory->create(new OrderType(), $order, array('method' => $method));
        $form->submit($parameters, 'PATCH' !== $method);

        if ($form->isValid()) {
            
            $order = $form->getData();
            $this->om->persist($order);
            $this->om->persist($order);
            $this->om->flush($order);

            return $order;
        }

        // debug form
        //var_dump($form->getErrorsAsString());

        throw new InvalidFormException('Invalid submitted data: ' . $form->getErrorsAsString(), $form);
    }

    private function createOrder()
    {
        return new $this->entityClass();
    }
}
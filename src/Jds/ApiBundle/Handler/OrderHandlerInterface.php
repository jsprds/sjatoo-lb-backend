<?php

namespace Jds\ApiBundle\Handler;

use Jds\ApiBundle\Model\OrderInterface;

interface OrderHandlerInterface
{
    /**
     * Get a Order given the identifier
     *
     * @api
     *
     * @param mixed $id
     *
     * @return OrderInterface
     */
    public function get($id);

    /**
     * Get a list of orders.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 5, $offset = 0, $orderby = null);

    /**
     * Post Order, creates a new Order.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return OrderInterface
     */
    public function post(array $parameters);

    /**
     * Edit a Order.
     *
     * @api
     *
     * @param OrderInterface   $product
     * @param array           $parameters
     *
     * @return OrderInterface
     */
    public function put(OrderInterface $product, array $parameters);

    /**
     * Partially update a Order.
     *
     * @api
     *
     * @param OrderInterface   $product
     * @param array           $parameters
     *
     * @return OrderInterface
     */
    public function patch(OrderInterface $product, array $parameters);
}
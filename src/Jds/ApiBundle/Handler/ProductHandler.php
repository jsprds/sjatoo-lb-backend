<?php

namespace Jds\ApiBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use Jds\ApiBundle\Model\ProductInterface;
use Jds\ApiBundle\Form\Type\ProductType;
use Jds\ApiBundle\Exception\InvalidFormException;

class ProductHandler implements ProductHandlerInterface
{
    private $om;
    private $entityClass;
    private $repository;
    private $formFactory;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory)
    {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
    }

    /**
     * Get a Product.
     *
     * @param mixed $id
     *
     * @return ProductInterface
     */
    public function get($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Get a list of Products.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 25, $offset = 0, $orderBy = 'p.id')
    {
       /// Start query
        $qb = $this->om->getRepository($this->entityClass)->createQueryBuilder('p');

        /* SET ORDER BY */
        if($orderBy != null) {
            $sort = (strpos($orderBy, '-') === 0 ? 'DESC' : 'ASC');
            $orderBy = ltrim($orderBy, '-');
            $qb->orderBy($orderBy, $sort);
        }

        $qb->setFirstResult($offset);
        $qb->setMaxResults($limit);

        $query =  $qb->getQuery();

        return $query->execute();
    }

    /**
     * Create a new Product.
     *
     * @param array $parameters
     *
     * @return ProductInterface
     */
    public function post(array $parameters)
    {
        $product = $this->createProduct();

        return $this->processForm($product, $parameters, 'POST');
    }

    /**
     * Edit a Product.
     *
     * @param ProductInterface $product
     * @param array         $parameters
     *
     * @return ProductInterface
     */
    public function put(ProductInterface $product, array $parameters)
    {
        return $this->processForm($product, $parameters, 'PUT');
    }

    /**
     * Partially update a Product.
     *
     * @param ProductInterface $product
     * @param array         $parameters
     *
     * @return ProductInterface
     */
    public function patch(ProductInterface $product, array $parameters)
    {
        return $this->processForm($product, $parameters, 'PATCH');
    }

    /**
     * Processes the form.
     *
     * @param ProductInterface $product
     * @param array         $parameters
     * @param String        $method
     *
     * @return ProductInterface
     *
     * @throws \Jds\ProductBundle\Exception\InvalidFormException
     */
    private function processForm(ProductInterface $product, array $parameters, $method = "PUT")
    {
        $form = $this->formFactory->create(new ProductType(), $product, array('method' => $method));
        $form->submit($parameters, 'PATCH' !== $method);
        if ($form->isValid()) {
            
            $product = $form->getData();
            $this->om->persist($product);
            $this->om->flush($product);

            return $product;
        }

        throw new InvalidFormException('Invalid submitted data', $form);
    }

    private function createProduct()
    {
        return new $this->entityClass();
    }

}
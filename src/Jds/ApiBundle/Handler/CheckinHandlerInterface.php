<?php

namespace Jds\ApiBundle\Handler;

use Jds\ApiBundle\Model\CheckinInterface;

interface CheckinHandlerInterface
{
    /**
     * Get a Checkin given the identifier
     *
     * @api
     *
     * @param mixed $id
     *
     * @return CheckinInterface
     */
    public function get($id);

    /**
     * Get a list of orders.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 5, $offset = 0, $orderby = null);

    /**
     * Post Checkin, creates a new Checkin.
     *
     * @api
     *
     * @param array $parameters
     *
     * @return CheckinInterface
     */
    public function post(array $parameters);

    /**
     * Edit a Checkin.
     *
     * @api
     *
     * @param CheckinInterface   $product
     * @param array           $parameters
     *
     * @return CheckinInterface
     */
    public function put(CheckinInterface $product, array $parameters);

    /**
     * Partially update a Checkin.
     *
     * @api
     *
     * @param CheckinInterface   $product
     * @param array           $parameters
     *
     * @return CheckinInterface
     */
    public function patch(CheckinInterface $product, array $parameters);
}
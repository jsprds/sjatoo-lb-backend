<?php

namespace Jds\ApiBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use Jds\ApiBundle\Model\CheckinInterface;
use Jds\ApiBundle\Form\Type\CheckinType;
use Jds\ApiBundle\Exception\InvalidFormException;

class CheckinHandler implements CheckinHandlerInterface
{
    private $om;
    private $entityClass;
    private $repository;
    private $formFactory;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory)
    {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
    }

    /**
     * Get a Checkin.
     *
     * @param mixed $id
     *
     * @return CheckinInterface
     */
    public function get($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Get a list of Checkins.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 25, $offset = 0, $orderBy = 'id', $criteria = array())
    {
        /// Start query
        $qb = $this->om->getRepository($this->entityClass)->createQueryBuilder('e');

        // criteria
        if(count($criteria) > 0) {
            if(isset($criteria['date'])) {
                $qb->andWhere("e.startDate < :eventDate")
                   ->andWhere("e.endDate > :eventDate")
                   ->setParameter("eventDate", $criteria['date']->format('Y-m-d H:i:s'));
            }
        }

        /* SET ORDER BY */
        if($orderBy != null) {
            $sort = (strpos($orderBy, '-') === 0 ? 'DESC' : 'ASC');
            $orderBy = 'e.' . ltrim($orderBy, '-');
            $qb->orderBy($orderBy, $sort);
        }

        $qb->setFirstResult($offset);
        $qb->setMaxResults($limit);

        $query =  $qb->getQuery();

        return $query->execute();
    }

    /**
     * Create a new Checkin.
     *
     * @param array $parameters
     *
     * @return CheckinInterface
     */
    public function post(array $parameters)
    {
        $checkin = $this->createCheckin();

        return $this->processForm($checkin, $parameters, 'POST');
    }

    /**
     * Partially update a Checkin.
     *
     * @param CheckinInterface $checkin
     * @param array         $parameters
     *
     * @return CheckinInterface
     */
    public function patch(CheckinInterface $event, array $parameters)
    {
        return $this->processForm($event, $parameters, 'PATCH');
    }

    /**
     * Edit a Checkin.
     *
     * @param CheckinInterface    $event
     * @param array             $parameters
     *
     * @return CheckinInterface
     */
    public function put(CheckinInterface $event, array $parameters)
    {
        return $this->processForm($event, $parameters, 'PUT');
    }

    /**
     * Processes the form.
     *
     * @param CheckinInterface    $event
     * @param array             $parameters
     * @param String            $method
     *
     * @return CheckinInterface
     *
     * @throws \Jds\CheckinBundle\Exception\InvalidFormException
     */
    private function processForm(CheckinInterface $event, array $parameters, $method = "PUT")
    {
        $form = $this->formFactory->create(new CheckinType(), $event, array('method' => $method));
        $form->submit($parameters, 'PATCH' !== $method);

        if ($form->isValid()) {
            
            $event = $form->getData();
            $this->om->persist($event);
            $this->om->persist($event);
            $this->om->flush($event);

            return $event;
        }

        // debug form
        var_dump($form->getErrorsAsString());

        throw new InvalidFormException('Invalid submitted data: ' . $form->getErrorsAsString(), $form);
    }

    private function createCheckin()
    {
        return new $this->entityClass();
    }
}